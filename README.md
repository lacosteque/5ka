
## Name
5ka Scraper


## Descriprtion
Simple asynchronous CLI scraper for IOS mobile app 
[dostavka.5ka.ru](https://dostavka.5ka.ru).
The scraper can cache requests and use HTTP | SOCKS5 proxy servers. 
The settings are in the file config.py

### Project tree

```
5ka
├─ .gitignore
├─ README.md
├─ app
│  ├─ __init__.py
│  ├─ api
│  │  ├─ geo
│  │  │  ├─ geo.py
│  │  │  └─ nominatim.py
│  │  └─ mobile_app
│  │     ├─ app.py
│  │     └─ headers.py
│  ├─ client
│  │  ├─ cache.py
│  │  └─ http.py
│  ├─ core
│  │  ├─ config.py
│  │  ├─ exceptions.py
│  │  └─ schemas
│  │     ├─ http.py
│  │     ├─ mobile_app.py
│  │     └─ nominatim.py
│  ├─ export
│  │  └─ csv.py
│  ├─ main.py
├─ poetry.lock
├─ pyproject.toml
├─ screenshots
│  ├─ done.png
│  └─ error.png
└─ tests
   ├─ __init__.py
   └─ test_5ka.py

```


## Install prerequisites

 - [Git](http://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
 - [Poetry](https://python-poetry.org/docs/)


## Usage

Clone git repository:

``` sh
    $ git clone https://gitlab.com/lacosteque/5ka.git
```

Go to the project directory:

``` sh
    $ cd 5ka
```

Install project dependencies:
``` sh
    $ poetry install
```

Go to App folder:
``` sh
    $ cd app
```

Run main.py

``` sh
    $ poetry shell
    $ python main.py 
    or
    $ poetry run python main.py
```

- Scrape [Done!]

![Done](https://gitlab.com/lacosteque/5ka/-/raw/main/screenshots/done.png)

- Scrape [Error!]

![Error](https://gitlab.com/lacosteque/5ka/-/raw/main/screenshots/error.png)

#### To Do

 - work through the exceptions
 - add Android device emulation
 - make web service based on Fast API  

