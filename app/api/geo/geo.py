from abc import ABC, abstractmethod
from dataclasses import dataclass


@dataclass(slots=True, frozen=True)
class Coordinates:
    latitude: float
    longitude: float


class BaseGeo(ABC):
    @abstractmethod
    async def get_coordinates(self):
        pass
