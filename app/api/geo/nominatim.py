from typing import ClassVar

from api.geo.geo import BaseGeo, Coordinates
from client.http import HTTPClient
from core.config import settings
from core.exceptions import CantGetCoordinates
from core.schemas.http import SchemaHttpGetData
from core.schemas.nominatim import SchemaNominatimResponse as ApiResponse
from pydantic import ValidationError


class Nominatim(BaseGeo):

    API_URL: ClassVar[str] = 'https://nominatim.openstreetmap.org/search'
    COUNTRY: ClassVar[str] = 'Россия'
    FORMAT: ClassVar[str] = 'geocodejson'

    def __init__(self, street: str | None, city: str | None) -> None:
        if not street:
            street = settings.DEFAULT_ADRESS
        if not city:
            city = settings.DEFAULT_CITY

        self.params = {
            'country': self.COUNTRY,
            'street': street,
            'city': city,
            'format': self.FORMAT,
        }

    async def get_coordinates(self, client: HTTPClient) -> Coordinates:
        data = SchemaHttpGetData.construct(
            url=self.API_URL, params=self.params
        )

        response = await client.request(**data.dict())
        try:
            data = ApiResponse.parse_raw(response.text)
        except ValidationError as exc:
            raise CantGetCoordinates(
                f'api validation error: {exc.json()}'
            ) from exc
        else:
            if not data.features:
                raise CantGetCoordinates("Can't get current GPS coordinates")

            latitude = data.features[0].geometry.coordinates[1]
            longitude = data.features[0].geometry.coordinates[0]
            return Coordinates(latitude, longitude)
