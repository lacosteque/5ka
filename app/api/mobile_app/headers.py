import random
import uuid
from typing import ClassVar

from core.config import settings


class HTTPHeaders:

    HOST: ClassVar[str] = '5d.5ka.ru'
    ACCEPT: ClassVar[str] = 'application/json'
    ACCEPT_LANGUAGE: ClassVar[str] = 'en-RU;q=1.0, ru-RU;q=0.9'
    X_PACKAGE_NAME: ClassVar[str] = 'com.antimarket.pyaterochkadelivery'
    USER_AGENT: ClassVar[
        str
    ] = f"""delivery_release/{settings.APP_VERSION} \
    (com.antimarket.pyaterochkadelivery; \
            build:1; \
            iOS {random.choice(settings.IOS_VERSIONS)}) Alamofire/5.6.1'"""
    X_CAN_RECEIVE_PUSH: ClassVar[str] = 'false'
    PRAGMA: ClassVar[str] = 'no-cache'
    CACHE_CONTROLE: ClassVar[str] = 'no-cache'
    X_PLATFORM: ClassVar[str] = 'ios'
    DEVICE_ID: ClassVar[uuid.UUID] = uuid.uuid4()
    ACCEPT_ENCODING: ClassVar[str] = 'gzip, deflate'
    CONNECTION: ClassVar[str] = 'close'

    async def make_headers(self, x_store: str | None = None) -> dict:

        headers = {
            'Host': self.HOST,
            'Accept': self.ACCEPT,
            'Accept-Language': self.ACCEPT_LANGUAGE,
            'Accept-Encoding': self.ACCEPT_ENCODING,
            'X-PACKAGE-NAME': self.X_PACKAGE_NAME,
            'User-Agent': self.USER_AGENT,
            'X-DEVICE-ID': str(self.DEVICE_ID).upper(),
            'X-APP-VERSION': settings.APP_VERSION,
            'X-PLATFORM': self.X_PLATFORM,
            'X-CAN-RECEIVE-PUSH': self.X_CAN_RECEIVE_PUSH,
            'Pragma': self.PRAGMA,
            'Cache-Control': self.CACHE_CONTROLE,
            'Connection': self.CONNECTION,
        }

        if x_store:
            headers['X-USER-STORE'] = x_store

        return headers
