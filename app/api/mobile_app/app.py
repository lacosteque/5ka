from itertools import chain
from typing import AsyncGenerator, Iterable

from api.geo.geo import Coordinates
from api.geo.nominatim import Nominatim
from api.mobile_app.headers import HTTPHeaders
from client.http import HTTPClient
from core.exceptions import (ApiServiceError, ApiValidationError,
                             CantGetCoordinates)
from core.schemas.http import SchemaHttpGetData
from core.schemas.mobile_app import ProductPlu
from core.schemas.mobile_app import \
    SchemaApiResponseListingCategorys as Category
from core.schemas.mobile_app import \
    SchemaApiResponseListingProducts as ListingProduct
from core.schemas.mobile_app import SchemaApiResponseNearestStore as Store
from core.schemas.mobile_app import SchemaApiResponseProduct as Product
from pydantic import ValidationError


class App5ka:
    def __init__(
        self, client: HTTPClient, headers: HTTPHeaders, geocoder: Nominatim
    ) -> None:
        self.headers = headers
        self.geocoder = geocoder
        self.client = client

    async def get_nearest_store(self) -> Store:
        url = 'https://5d.5ka.ru/api/orders/v1/orders/stores/'
        headers = await self.headers.make_headers()

        try:
            coordinates = await self.geocoder.get_coordinates(self.client)
        except CantGetCoordinates as exc:
            print(
                f'\n \033[31m[!]\033[0m - {exc}. Please provide your coordinates [ex. latitude: 55.752460 | longitude: 37.617780 ]\n'
            )

            latitude = float(input(' \033[33m[?]\033[0m - Enter latitude: '))
            longitude = float(input(' \033[33m[?]\033[0m - Enter longitude: '))

            coordinates = Coordinates(latitude, longitude)

        params = {
            'lat': coordinates.latitude,
            'lon': coordinates.longitude,
        }

        data = SchemaHttpGetData.construct(
            url=url, headers=headers, params=params
        )

        response = await self.client.request(**data.dict())
        try:
            data = Store.parse_raw(response.text)
        except ValidationError as exc:
            raise ApiValidationError(
                f'api validation error : {url} - {exc.json()}'
            ) from exc
        else:
            print(
                f'\n \033[32m[+]\033[0m - Nearest store: {data.shop_address}\n'
            )
            return data

    async def get_listing_categorys(self, store: Store) -> Iterable[Category]:
        url = 'https://5d.5ka.ru/api/cita/v5/categories/'
        headers = await self.headers.make_headers(store.sap_code)
        data = SchemaHttpGetData.construct(url=url, headers=headers)
        response = await self.client.request(**data.dict())
        try:
            categorys = (
                Category.parse_obj(category) for category in response.json()
            )

        except ValidationError as exc:
            raise ApiValidationError(
                f'api validation error : {url} - {exc.json()}'
            ) from exc
        else:
            return categorys

    async def _get_listing_products(
        self, store: Store, values: AsyncGenerator
    ) -> Iterable[ProductPlu]:
        url = 'https://5d.5ka.ru/api/cita/v1/products/'

        data_tasks = []
        limit = 15
        offset = 0

        async for id, qty in values:

            if limit > qty:
                limit = qty

            if qty % limit:
                pagination = int(qty / limit) + 1
            else:
                pagination = int(qty / limit)

            for item in range(pagination):
                offset = limit * item
                headers = await self.headers.make_headers(store.sap_code)
                params = {
                    'category': id,
                    'limit': limit,
                    'offset': offset,
                }

                data = SchemaHttpGetData.construct(
                    url=url, headers=headers, params=params
                )
                data_tasks.append(data)

        responses = await self.client.http_tasks(data_tasks)

        try:
            products_plu = (
                ListingProduct.parse_raw(response.text).products
                for response in responses
            )
        except ValidationError as exc:
            raise ApiValidationError(
                f'api validation error : {url} - {exc.json()}'
            ) from exc
        except ApiServiceError:
            pass
        else:
            return list(chain.from_iterable(products_plu))

    async def get_product_info(
        self, store: Store, values: AsyncGenerator
    ) -> Iterable[Product]:
        products_plu = await self._get_listing_products(store, values)

        data_tasks = []

        for product in products_plu:
            url = f'https://5d.5ka.ru/api/search/v1/products/{product.plu}'
            params = {
                'sap_code': store.sap_code,
            }
            headers = await self.headers.make_headers(store.sap_code)
            data = SchemaHttpGetData.construct(
                url=url, headers=headers, params=params
            )
            data_tasks.append(data)

        responses = await self.client.http_tasks(data_tasks)

        try:
            output = (
                Product.parse_raw(response.text) for response in responses
            )
        except ValidationError as exc:
            raise ApiValidationError(
                f'api validation error : {exc.json()}'
            ) from exc
        else:
            return output
