from pydantic import BaseModel


class Geometry(BaseModel):
    coordinates: list[float]


class Features(BaseModel):
    geometry: Geometry


class SchemaNominatimResponse(BaseModel):
    features: list[Features]
