from typing import Literal

from pydantic import BaseModel as PydanticBaseModel
from pydantic import Field, HttpUrl


class BaseModel(PydanticBaseModel):
    def dict(self, by_alias=True, exclude_none=True, **kwargs):
        return super().dict(
            by_alias=by_alias, exclude_none=exclude_none, **kwargs
        )


class SchemaHttpBaseData(BaseModel):
    url: HttpUrl | str
    headers: dict | None = None


class SchemaHttpGetData(SchemaHttpBaseData):
    method: Literal['GET'] = 'GET'
    params: dict | None = None


class SchemaHttpPostData(SchemaHttpBaseData):
    method: Literal['POST'] = 'POST'
    data: str | None = None
    json_obj: dict = Field(None, alias='json')
