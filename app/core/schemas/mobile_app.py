from pydantic import AnyHttpUrl, BaseModel


class SchemaApiResponseNearestStore(BaseModel):
    has_delivery: bool
    sap_code: str
    shop_address: str
    store_city: str
    store_id: int


class SchemaApiResponseListingCategorys(BaseModel):
    name: str
    id: int
    products_count: int


class ProductPlu(BaseModel):
    plu: int


class PreviewData(BaseModel):
    products: list[ProductPlu]


class SchemaApiResponseListingProducts(BaseModel):
    products: list[ProductPlu]


class ProductPrice(BaseModel):
    discount: int | None = None
    price_discount: float | None = None
    price_regular: float


class SchemaApiResponseProduct(BaseModel):
    name: str
    image: AnyHttpUrl
    uom: str
    prices: ProductPrice
