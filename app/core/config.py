import asyncio
from functools import lru_cache


class CsvSettings:
    ENCODING = 'UTF-8'
    DELIMITER = '\t'


class DefaultGeo:
    DEFAULT_CITY: str = 'Санкт-Петербург'
    DEFAULT_ADRESS: str = '8-я Советская 50'


class DeviceSettings:
    APP_VERSION: str = '4.3.6'
    IOS_VERSIONS: tuple = (
        '15.0',
        '15.0.1',
        '15.0.2',
        '15.1',
        '15.1.1',
        '15.2',
        '15.2.1',
        '15.3',
        '15.3.1',
        '15.4',
        '15.4.1',
        '15.5',
        '15.6',
        '15.6.1',
        '15.7',
        '16.0',
        '16.0.1',
        '16.0.2',
        '16.0.3',
    )


class HTTPClientSettings:
    """
    Proxy example URI
    PROXY_HTTP_HTTPS: http://localhost:8030
    PROXY_SOCKS5: socks5://user:password@127.0.0.1:1080
    """

    HTTP_CACHE: bool = False
    HTTP2: bool = True
    MAX_KEEPALIVE_CONNECTION: int = 10
    MAX_CONNECTIONS: int = 20
    TIMEOUT: int = 10
    PROXY_HTTP_HTTPS: str | None = None
    PROXY_SOCKS5: str | None = None
    VERIFY: bool = False


class Settings(DeviceSettings, HTTPClientSettings, DefaultGeo, CsvSettings):
    pass


@lru_cache()
async def get_settings():
    return Settings()


settings = asyncio.run(get_settings())
