class CustomException(Exception):
    def __init__(self, message):
        super().__init__(message)


class CantGetCoordinates(CustomException):
    """Can't get GPS coordinates"""


class ApiServiceError(CustomException):
    """Scraper can't current response"""


class ApiValidationError(CustomException):
    """Bad data from external API"""
