import csv
from datetime import datetime
from typing import Iterable

from core.config import settings
from core.schemas.mobile_app import SchemaApiResponseProduct as Product


async def export_csv(items: Iterable[Product]) -> None:
    dt = datetime.now()
    timestamp = int(datetime.timestamp(dt))
    current_date = dt.date()

    with open(
        f'result-{current_date}-{timestamp}.csv',
        'w',
        newline='',
        encoding=settings.ENCODING,
    ) as csvfile:
        fieldnames = [
            'name',
            'image',
            'uom',
            'discount',
            'price_discount',
            'price_regular',
        ]
        writer = csv.DictWriter(
            csvfile, fieldnames=fieldnames, delimiter=settings.DELIMITER
        )

        writer.writeheader()

        for item in items:
            writer.writerow(
                {
                    'name': item.name,
                    'image': item.image,
                    'uom': item.uom,
                    'discount': item.prices.discount,
                    'price_discount': item.prices.price_discount,
                    'price_regular': item.prices.price_regular,
                }
            )
