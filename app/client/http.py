import asyncio
from typing import ClassVar, Iterable

from core.config import settings
from core.exceptions import ApiServiceError
from core.schemas.http import SchemaHttpGetData, SchemaHttpPostData
from httpx import AsyncClient, HTTPError, Limits, Response
from httpx_cache import AsyncClient as AsyncCachedClient
from httpx_socks import AsyncProxyTransport


class HTTPClient:

    CLIENT_CONFIG: ClassVar[dict] = {
        'timeout': settings.TIMEOUT,
        'limits': Limits(
            max_keepalive_connections=settings.MAX_KEEPALIVE_CONNECTION,
            max_connections=settings.MAX_CONNECTIONS,
        ),
        'verify': settings.VERIFY,
    }

    def __init__(self) -> None:
        if settings.PROXY_HTTP_HTTPS:
            self.CLIENT_CONFIG['proxies'] = settings.PROXY_HTTP_HTTPS
        if settings.PROXY_SOCKS5:
            self.CLIENT_CONFIG['transport'] = AsyncProxyTransport.from_url(
                settings.PROXY_SOCKS5
            )
        if settings.HTTP2:
            self.CLIENT_CONFIG['http2'] = settings.HTTP2
        if settings.HTTP_CACHE:
            self.client = AsyncCachedClient(**self.CLIENT_CONFIG)
        else:
            self.client = AsyncClient(**self.CLIENT_CONFIG)

    async def request(self, **kwargs: str) -> Response:
        response = await self.client.request(**kwargs)
        try:
            response.raise_for_status()
        except HTTPError as exc:
            raise ApiServiceError(exc) from exc
        else:
            return response

    async def http_tasks(
        self, http_data: Iterable[SchemaHttpGetData | SchemaHttpPostData]
    ) -> Iterable[Response]:

        tasks = map(
            lambda data: asyncio.create_task(self.request(**data.dict())),
            http_data,
        )

        responses = tuple(
            filter(
                lambda response: isinstance(response, Response),
                await asyncio.gather(*tasks, return_exceptions=True),
            )
        )
        if responses:
            return responses
        raise ApiServiceError('All responses is BAD!')
