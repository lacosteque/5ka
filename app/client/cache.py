from typing import AsyncGenerator, Iterable

from aiocache import Cache
from core.schemas.mobile_app import \
    SchemaApiResponseListingCategorys as Category

cache = Cache(Cache.MEMORY)


class CacheClient:
    @staticmethod
    async def set_cache(items: Iterable[Category]):
        print('\n \033[32m[Assortment]\033[0m \n')
        i = 0
        for item in items:
            i += 1
            await cache.set(i, (item.id, item.products_count))
            print(
                f' \033[32m[+]\033[0m - {i}. {item.name} [{item.products_count} ед.]'
            )

    @staticmethod
    async def get_cache(keys: Iterable[int]) -> AsyncGenerator:
        return (await cache.get(key) for key in keys)
