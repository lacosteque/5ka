import asyncio

from api.geo.nominatim import Nominatim
from api.mobile_app.app import App5ka
from api.mobile_app.headers import HTTPHeaders as Headers
from client.cache import CacheClient
from client.http import HTTPClient
from core.config import settings
from core.exceptions import ApiValidationError
from export.csv import export_csv
from revolution import Revolution

print('\n \033[32m[Information]\033[0m \n')

print(
    f"""To start parsing, you need to specify the delivery address. The nearest store will be found and data will be collected based on its assortment. If you leave the fields empty, the standard settings will be taken: \n- city: {settings.DEFAULT_CITY} 
- address: {settings.DEFAULT_ADRESS}"""
)
city = input(
    f'\n \033[33m[?]\033[0m - Enter city name [ex. {settings.DEFAULT_CITY}]: '
)
street = input(
    f' \033[33m[?]\033[0m - Enter adress [ex. {settings.DEFAULT_ADRESS}]: '
).replace(' ', '+')

if not all((city, street)):
    city = None
    street = None

client = HTTPClient()

app = App5ka(
    client=client,
    headers=Headers(),
    geocoder=Nominatim(street=street, city=city),
)


async def main():
    try:
        store = await app.get_nearest_store()
    except ApiValidationError:
        print("\n \033[31m[!]\033[0m - Couldn't get the nearest store")
        exit(1)

    try:
        with Revolution(desc='Scraping store & assortment...') as _:
            items = await app.get_listing_categorys(store)
        await CacheClient.set_cache(items)
    except ApiValidationError:
        pass

    keys = list(
        map(
            int,
            input(
                '\n \033[33m[?]\033[0m - Enter category ID (ex. 1 3 5 etc.): '
            ).split(),
        )
    )

    values = await CacheClient.get_cache(keys)

    try:
        print('\n')
        with Revolution(desc='Scraping data...') as _:
            items = await app.get_product_info(store, values)

    except ApiValidationError:
        pass

    else:
        with Revolution(desc='Export file...') as _:
            await export_csv(items)
        print('\n \033[32m[!]\033[0m - Done!')


if __name__ == '__main__':
    asyncio.run(main())
